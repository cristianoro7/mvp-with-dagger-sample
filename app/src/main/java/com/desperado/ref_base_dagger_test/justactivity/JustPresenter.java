package com.desperado.ref_base_dagger_test.justactivity;

import android.content.Context;

import com.desperado.ref_base_dagger_test.app.SingleClassInApp;
import com.desperado.ref_base_dagger_test.di.qualifier.ActivityContext;
import com.desperado.ref_base_dagger_test.di.scope.PerActivity;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Created by root on 17-5-2.
 */
@PerActivity
public class JustPresenter extends JustActivityWithPContract.Presenter {

    private String mName;
    private SingleClassInApp mSingleClassInApp;

    @Inject
    public JustPresenter(@ActivityContext Context context, JustActivityWithPContract.View view,
                         @Named("nameInActivity") String mName, SingleClassInApp singleClassInApp) {
        super(context, view);
        this.mName = mName;
        this.mSingleClassInApp = singleClassInApp;
    }

    @Override
    void getName() {
        getView().showName(mName);
    }

    @Override
    void showSingleName() {
        getView().showName(mSingleClassInApp.getName());
    }
}
