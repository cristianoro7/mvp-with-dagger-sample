package com.desperado.ref_base_dagger_test;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.desperado.ref_base_dagger_test.bothhasp.BothHasPActivity;
import com.desperado.ref_base_dagger_test.commonp.SharePActivity;
import com.desperado.ref_base_dagger_test.justactivity.JustActivityWithP;
import com.desperado.ref_base_dagger_test.justfragment.JustFragmentPActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void justActivityWithP(View view) {
        JustActivityWithP.start(this);
    }

    public void shareP(View view) {
        SharePActivity.start(this);
    }

    public void fragmentP(View view) {
        JustFragmentPActivity.start(this);
    }

    public void bothHasP(View view) {
        BothHasPActivity.start(this);
    }
}
