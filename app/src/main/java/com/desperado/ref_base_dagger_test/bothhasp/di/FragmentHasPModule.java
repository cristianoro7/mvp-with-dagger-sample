package com.desperado.ref_base_dagger_test.bothhasp.di;

import android.support.v4.app.Fragment;

import com.desperado.ref_base_dagger_test.bothhasp.FragmentContract;
import com.desperado.ref_base_dagger_test.di.scope.PerFragment;

import dagger.Module;
import dagger.Provides;

/**
 * Created by root on 17-5-4.
 */
@Module
public class FragmentHasPModule {

    @Provides
    @PerFragment
    FragmentContract.View providesView(Fragment fragment) {
        return (FragmentContract.View) fragment;
    }
}
