package com.desperado.ref_base_dagger_test.bothhasp.di;

import com.desperado.ref_base_dagger_test.app.di.CustomApplicationComponent;
import com.desperado.ref_base_dagger_test.bothhasp.BothHasPFragment;
import com.desperado.ref_base_dagger_test.di.scope.PerFragment;

import dagger.Component;

/**
 * Created by root on 17-5-4.
 */
@PerFragment
@Component(dependencies = CustomApplicationComponent.class, modules = {com.desperado.ref_base_dagger_test.di.module.FragmentModule.class, FragmentHasPModule.class})
public interface FragmentComponent {
    void inject(BothHasPFragment fragment);
}
