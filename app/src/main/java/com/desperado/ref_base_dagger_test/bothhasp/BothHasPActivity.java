package com.desperado.ref_base_dagger_test.bothhasp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.desperado.ref_base_dagger_test.R;
import com.desperado.ref_base_dagger_test.bothhasp.di.DaggerActivityComponent;
import com.desperado.ref_base_dagger_test.mvpwithdagger.MSBaseDaggerMvpActivity;

/**
 * Created by root on 17-5-4.
 */

public class BothHasPActivity extends MSBaseDaggerMvpActivity<ActivityPresenter> implements ActivityContract.View {

    @Override
    protected void setupComponent() {
        DaggerActivityComponent.builder()
                .customApplicationComponent(getCustomAppComponent())
                .activityModule(getAcModule())
                .build()
                .inject(this);
    }

    @Override
    protected void init(ActivityPresenter presenter, Bundle savedInstanceState) {
        setContentView(R.layout.activity_both_has_p);
    }

    public void showNameInBothP(View view) {
        getPresenter().showName();
    }

    public void createFragment(View view) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, new BothHasPFragment())
                .commit();
    }

    public static void start(Context context) {
        Intent starter = new Intent(context, BothHasPActivity.class);
        context.startActivity(starter);
    }

    @Override
    public void showName(String name) {
        Toast.makeText(this, name, Toast.LENGTH_SHORT).show();
    }
}
