package com.desperado.ref_base_dagger_test.bothhasp.di;

import com.desperado.ref_base_dagger_test.app.di.CustomApplicationComponent;
import com.desperado.ref_base_dagger_test.bothhasp.BothHasPActivity;
import com.desperado.ref_base_dagger_test.di.scope.PerActivity;

import dagger.Component;

/**
 * Created by root on 17-5-4.
 */
@PerActivity
@Component(dependencies = CustomApplicationComponent.class, modules = {com.desperado.ref_base_dagger_test.di.module.ActivityModule.class, ActivityHasPModule.class})
public interface ActivityComponent {
    void inject(BothHasPActivity activity);
}
