package com.desperado.ref_base_dagger_test.di.module;

import android.content.Context;
import android.support.v4.app.Fragment;

import com.desperado.ref_base_dagger_test.di.scope.PerFragment;

import dagger.Module;
import dagger.Provides;

/**
 * Created by desperado on 17-4-30.
 * 注意: 在写providesXX方法时, 记得要添加{@link PerFragment}注解, 否则Dagger生成的实例并不是局部单例
 */
@Module
public class FragmentModule {

    private final Context context;

    private final Fragment fragment;

    public FragmentModule(Context context, Fragment fragment) {
        this.context = context;
        this.fragment = fragment;
    }

    @Provides
    @PerFragment
    Context providesContext() {
        return context;
    }

    @Provides
    @PerFragment
    Fragment providesFragment() {
        return fragment;
    }
}
