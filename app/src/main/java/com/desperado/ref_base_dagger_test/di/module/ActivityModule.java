package com.desperado.ref_base_dagger_test.di.module;

import android.app.Activity;
import android.content.Context;

import com.desperado.ref_base_dagger_test.di.qualifier.ActivityContext;
import com.desperado.ref_base_dagger_test.di.scope.PerActivity;

import dagger.Module;
import dagger.Provides;



/**
 * Created by desperado on 17-4-27.
 * 注意: 在写providesXX方法时, 记得要添加{@link com.desperado.ref_base_dagger_test.di.scope.PerActivity}注解, 否则Dagger生成的实例并不是局部单例
 */
@Module
public class ActivityModule {

    private final Activity mActivity;

    public ActivityModule(Activity mActivity) {
        this.mActivity = mActivity;
    }

    @Provides
    @PerActivity
    @ActivityContext
    Context providesActivityContext() {
        return mActivity;
    }

    @Provides
    @PerActivity
    Activity providesActivity() {
        return mActivity;
    }
}
