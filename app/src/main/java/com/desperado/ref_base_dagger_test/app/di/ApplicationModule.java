package com.desperado.ref_base_dagger_test.app.di;

import com.desperado.ref_base_dagger_test.app.NetworkManager;
import com.desperado.ref_base_dagger_test.app.SingleClassInApp;
import com.desperado.ref_base_dagger_test.app.DbManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by root on 17-5-2.
 */
@Module
public class ApplicationModule {

    @Provides
    @Singleton
    String providesName() {
        return "C罗";
    }


    @Provides
    @Singleton
    SingleClassInApp providesSingle() {
        return new SingleClassInApp();
    }

    @Singleton
    @Provides
    DbManager providesDbManager() {
        return new DbManager();
    }

    @Singleton
    @Provides
    NetworkManager providesNetworkManager() {
        return new NetworkManager();
    }
}
