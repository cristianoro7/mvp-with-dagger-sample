package com.desperado.ref_base_dagger_test.app.di;

import com.desperado.ref_base_dagger_test.app.NetworkManager;
import com.desperado.ref_base_dagger_test.app.SingleClassInApp;
import com.desperado.ref_base_dagger_test.app.TestApplication;
import com.desperado.ref_base_dagger_test.app.DbManager;
import com.desperado.ref_base_dagger_test.di.component.ApplicationComponent;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by root on 17-5-2.
 */
@Singleton
@Component(modules = {com.desperado.ref_base_dagger_test.di.module.ApplicationModule.class, ApplicationModule.class})
public interface CustomApplicationComponent extends ApplicationComponent {

    void inject(TestApplication application);

    SingleClassInApp getSingleClassInApp(); //暴露给依赖于CustomApplicationComponent的Component使用

    DbManager getDbManager();

    NetworkManager getNetworkManager();

}
