package com.desperado.ref_base_dagger_test.app;

import android.util.Log;

import com.desperado.ref_base_dagger_test.bestpractice.GlobalLogTag;

import javax.inject.Singleton;

/**
 * Created by root on 17-5-6.
 */
@Singleton
public class NetworkManager {

    public void fetchDataFromNetwork() {
        Log.d(GlobalLogTag.TAG, "fetchDataFromNetwork: ");
    }
}
