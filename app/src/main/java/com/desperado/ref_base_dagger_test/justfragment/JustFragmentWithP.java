package com.desperado.ref_base_dagger_test.justfragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.desperado.ref_base_dagger_test.R;
import com.desperado.ref_base_dagger_test.justfragment.di.DaggerJustFragmentComponent;
import com.desperado.ref_base_dagger_test.mvpwithdagger.MSBaseDaggerMvpFragment;

/**
 * Created by root on 17-5-4.
 */

public class JustFragmentWithP extends MSBaseDaggerMvpFragment<JustFragmentPresenter> implements JustFragmentWithPContract.View {

    @Override
    protected View createView(LayoutInflater inflater, ViewGroup container, Bundle bundle) {
        View view = inflater.inflate(R.layout.fragment_just_p, container, false);
        view.findViewById(R.id.just_bt_p).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getPresenter().showName();
            }
        });
        return view;
    }

    @Override
    protected void setupComponent() {
        DaggerJustFragmentComponent.builder()
                .customApplicationComponent(getCustomAppComponent())
                .fragmentModule(getFragmentModule())
                .build()
                .inject(this);
    }

    @Override
    protected void init(JustFragmentPresenter presenter, Bundle saveInstanceState) {

    }

    @Override
    public void showName(String name) {
        Toast.makeText(getActivity(), name, Toast.LENGTH_SHORT).show();
    }
}
