package com.desperado.ref_base_dagger_test.mvpwithdagger;

import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.desperado.ref_base_dagger_test.app.di.CustomApplicationComponent;
import com.desperado.ref_base_dagger_test.di.DaggerContract;
import com.desperado.ref_base_dagger_test.di.DaggerContractDelegate;
import com.desperado.ref_base_dagger_test.di.component.ApplicationComponent;
import com.desperado.ref_base_dagger_test.di.module.ActivityModule;

import javax.inject.Inject;


public abstract class MSBaseDaggerMvpActivity<P extends MSBaseRxContract.BasePresenter> extends AppCompatActivity
        implements DaggerContract.AppComponent, DaggerContract.AcModule,
        DaggerContract.CustomAppComponent<CustomApplicationComponent> {

    //以ClassName作为TAG
    private static final String TAG = MSBaseDaggerMvpActivity.class.getSimpleName();

    @Inject
    protected P mPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupComponent(); //注入依赖
        onCreatePresenter();
        init(mPresenter, savedInstanceState);
    }

    private void onCreatePresenter() {
        if (mPresenter != null) {
            mPresenter.onCreate();
        }
    }

    abstract protected void setupComponent();

    abstract protected void init(P presenter, Bundle savedInstanceState);

    final public P getPresenter() {
        return mPresenter;
    }

    @Override
    @CallSuper
    protected void onResume() {
        super.onResume();
        if (mPresenter != null)
            mPresenter.onResume();
    }

    @Override
    @CallSuper
    protected void onStop() {
        super.onStop();
        if (mPresenter != null)
            mPresenter.onStop();
    }

    @Override
    @CallSuper
    protected void onPause() {
        super.onPause();
        if (mPresenter != null)
            mPresenter.onPause();
    }

    @Override
    @CallSuper
    protected void onDestroy() {
        super.onDestroy();
        if (mPresenter != null) {
            mPresenter.onDestroy();
            mPresenter.detachView();
        }
    }

    @Override
    public ApplicationComponent getApplicationComponent() {
        return DaggerContractDelegate.getAppComponent(getApplication());
    }

    @Override
    public ActivityModule getAcModule() {
        return DaggerContractDelegate.getAcModule(this);
    }

    @Override
    public CustomApplicationComponent getCustomAppComponent() {
        return DaggerContractDelegate.getCustomAppComonent(getApplication(), CustomApplicationComponent.class);
    }
}

