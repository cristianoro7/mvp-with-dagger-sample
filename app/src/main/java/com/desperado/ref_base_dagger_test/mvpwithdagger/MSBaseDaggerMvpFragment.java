package com.desperado.ref_base_dagger_test.mvpwithdagger;

import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.desperado.ref_base_dagger_test.app.di.CustomApplicationComponent;
import com.desperado.ref_base_dagger_test.di.DaggerContract;
import com.desperado.ref_base_dagger_test.di.DaggerContractDelegate;
import com.desperado.ref_base_dagger_test.di.component.ApplicationComponent;
import com.desperado.ref_base_dagger_test.di.module.FragmentModule;

import javax.inject.Inject;

/**
 * * 关联{@link com.topview.xxt.base.component.mvp.MSBaseContract.BasePresenter }的生命周期
 * Created by robin on 17-2-12.
 * <p>
 * modified by desperado on 2017/5/2
 * 1.实现Dagger2注入P
 * 2.分别是实现{@link DaggerContract.com.topview.xxt.base.di.DaggerContract.AppComponent},
 * {@link DaggerContract.com.topview.xxt.base.di.DaggerContract.FragmentModule}和
 * {@link DaggerContract.com.topview.xxt.base.di.DaggerContract.AttachActivityComponent}, 用于获取
 * AppComponent, FragmentModule和宿主Activity的component
 */

public abstract class MSBaseDaggerMvpFragment<P extends MSBaseRxContract.BasePresenter> extends Fragment
        implements DaggerContract.AppComponent,
        DaggerContract.FragmentModule,
        DaggerContract.AttachActivityComponent,
DaggerContract.CustomAppComponent<CustomApplicationComponent> {


    private static final String TAG = MSBaseDaggerMvpFragment.class.getSimpleName();

    @Inject
    protected P mPresenter;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setupComponent();
        onCreatePresenter();
        init(mPresenter, savedInstanceState);
        return createView(inflater, container, savedInstanceState);
    }

    protected abstract View createView(LayoutInflater inflater, ViewGroup container, Bundle bundle);

    abstract protected void setupComponent();

    private void onCreatePresenter() {
        if (mPresenter != null) {
            mPresenter.onCreate();
        }
    }

    abstract protected void init(P presenter, Bundle saveInstanceState);

    public P getPresenter() {
        return mPresenter;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mPresenter != null)
            mPresenter.onResume();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mPresenter != null) {
            mPresenter.onStop();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mPresenter != null)
            mPresenter.onPause();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mPresenter != null) {
            mPresenter.onDestroy();
            mPresenter.detachView();
        }
    }

    @Override
    public ApplicationComponent getApplicationComponent() {
        return DaggerContractDelegate.getAppComponent(getActivity().getApplication());
    }

    @Override
    public <C> C getAttachActivityComponent(Class<C> classType) {
        return DaggerContractDelegate.getAttachActivityComponent(getActivity(), classType);
    }

    @Override
    public FragmentModule getFragmentModule() {
        return DaggerContractDelegate.getFragmentModule(getContext(), this);
    }

    @Override
    public CustomApplicationComponent getCustomAppComponent() {
        return DaggerContractDelegate.getCustomAppComonent(getActivity().getApplication(), CustomApplicationComponent.class);
    }
}
