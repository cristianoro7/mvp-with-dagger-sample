package com.desperado.ref_base_dagger_test.bestpractice;

import android.content.Context;

import com.desperado.ref_base_dagger_test.mvpwithdagger.MSBaseRxContract;

/**
 * Created by root on 17-5-4.
 */

public interface BestPracticeContract {

    interface View extends MSBaseRxContract.BaseView<Presenter> {

    }

    abstract class Presenter extends MSBaseRxContract.BasePresenter<View> {

        public Presenter(Context context, View view) {
            super(context, view);
        }

        public abstract void fetchData();

        public abstract void refresh();
    }
}
